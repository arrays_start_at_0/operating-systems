#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>

#define VARIANT "15640"

typedef int bool;
#define true 1
#define false 0

int currentOrBackDir(char* dirName) {
    if(strcmp(dirName, ".") == 0 || strcmp(dirName, "..") == 0) {
        return 1;
    }
    return 0;
}

int isDirectory(char *path) {
   struct stat statbuf;
   if (stat(path, &statbuf) != 0) {
       return 0;
   }
   return S_ISDIR(statbuf.st_mode);
}

int verifyStringEnd(char* entry, char* end) {
    if(strlen(entry) >= strlen(end) && !strcmp(entry + strlen(entry) - strlen(end), end)) {
        return 1;
    }
    return 0;
}

int verifyPerm(char* path, char* perm) {
    struct stat statbuf;
    if (stat(path, &statbuf) != 0) {
       return 0;
   }
    char permS[9];
    permS[0] = (statbuf.st_mode & S_IRUSR) ? 'r' : '-';
    permS[1] = (statbuf.st_mode & S_IWUSR) ? 'w' : '-';
    permS[2] = (statbuf.st_mode & S_IXUSR) ? 'x' : '-';
    permS[3] = (statbuf.st_mode & S_IRGRP) ? 'r' : '-';
    permS[4] = (statbuf.st_mode & S_IWGRP) ? 'w' : '-';
    permS[5] = (statbuf.st_mode & S_IXGRP) ? 'x' : '-';
    permS[6] = (statbuf.st_mode & S_IROTH) ? 'r' : '-';
    permS[7] = (statbuf.st_mode & S_IWOTH) ? 'w' : '-'; 
    permS[8] = (statbuf.st_mode & S_IXOTH) ? 'x' : '-';

   return strcmp(perm, permS) == 0;
}

void listFolders(char* path, bool recursive, char* end, char* perm) {
    DIR *dir = NULL;
    struct dirent *entry = NULL;
    char filePath[4096];

    dir = opendir(path);
    while((entry = readdir(dir)) != NULL) {
        if(!currentOrBackDir(entry->d_name)) {
            snprintf(filePath, 4096, "%s/%s", path, entry->d_name);
            if(end != NULL) {
                if(!verifyStringEnd(entry->d_name, end)) {
                    goto next;
                }
            }     
            if(perm[0] != '\0') {
                if(!verifyPerm(filePath, perm)) {
                    goto next;
                }
            }   
            
            printf("%s\n", filePath);

            next:
            if(recursive == true) {
                if(isDirectory(filePath)) {
                    listFolders(filePath, recursive, end, perm);
                }
            }
        }

    }
    closedir(dir);
}

int listCommand(int argc, char **argv) {
    char *path;
    char *temp;
    char *end;
    char perm[9] = {'\0'};
    bool recursive = false;

    for(int i = 1; i < argc; i++){
        temp = strstr(argv[i], "path=");
        if(temp != NULL) {
            path = temp + 5;//length of string "path="
        }
        temp = strstr(argv[i], "name_ends_with=");
        if(temp != NULL) {
            end = temp + 15;
        }
        temp = strstr(argv[i], "permissions=");
        if(temp != NULL) {
            strcpy(perm,temp + 12);
        }
        if(strcmp(argv[i], "recursive") == 0) {
            recursive = true;
        }
    }
    DIR *dir = NULL;
    dir = opendir(path);
    if(dir == NULL) {
        printf("ERROR\ninvalid directory path\n");
        return -1;
    }
    closedir(dir);

    printf("SUCCESS\n");
    listFolders(path, recursive, end, perm);
    return 0;
}
//--------------------------------------------------
typedef struct SF_Section {
    char name[21];
    short int type;
    int offset;
    int size;
} Section;


typedef struct SF_File {
   char magic;
   short int size;
   short int version;
   char nrSec; 
   Section *section;
} File;


///parses and returns read file with section headers
///if *SF_ret is NULL, the function won't print
///double negation !! transforms the expression into an int with values of either 0 or 1
///supposed to help the compiler understant what's going on
int parseCommand(char* path, File** SF_ret) {
    int file = -1;
    char retVal = -1;

    file = open(path, O_RDONLY);
    if(file == -1) {
        if(!!*SF_ret) perror("Could not open input file");
        goto cleanup;
    }
    File SF;
    SF.section = NULL;

    read(file, &SF.magic, 1);
    if(SF.magic != 'j') {
        if(!!*SF_ret) printf("ERROR\nwrong magic\n");
        goto cleanup;        
    }

    read(file, &SF.size, 2);

    read(file, &SF.version, 2);
    if(SF.version < 65 || SF.version > 117) {
        if(!!*SF_ret) printf("ERROR\nwrong version\n");
        goto cleanup;        
    }

    read(file, &SF.nrSec, 1);
    if(SF.nrSec < 5 || SF.nrSec > 16) {
        if(!!*SF_ret) printf("ERROR\nwrong sect_nr\n");
        goto cleanup;
    }
    
    SF.section = (Section*)malloc(SF.nrSec * sizeof(Section));
    for(int  i = 0; i < SF.nrSec; i++) {
        read(file, &SF.section[i].name, 20);
        SF.section[i].name[21] = '\0';
        read(file, &SF.section[i].type, 2);
        if(SF.section[i].type != 67 && SF.section[i].type != 24 && SF.section[i].type != 97) {
            if(!!*SF_ret) printf("ERROR\nwrong sect_types\n");
            goto cleanup;            
        }
        read(file, &SF.section[i].offset, 4);
        read(file, &SF.section[i].size, 4);
    }

    if(!!*SF_ret) {
        printf("SUCCESS\n");
        printf("version=%d\n", SF.version);
        printf("nr_sections=%d\n", SF.nrSec);
    }

    for(int i = 0 ; i < SF.nrSec; i++) {
        if(!!*SF_ret) printf("section%d: %s %d %d\n", i+1, SF.section[i].name, SF.section[i].type, SF.section[i].size);
    }
    retVal = 0;

    cleanup:
    *SF_ret = &SF;
    close(file);
    return retVal;
}
//----------------------------------------------------
void extract(int argc, char **argv) {
    char* temp;
    char* path;
    int secNr;
    int lineNr;

    for(int i = 1; i < argc; i++){
        temp = strstr(argv[i], "path=");
        if(temp != NULL) {
            path = temp + 5;//length of string "path="
        }
        temp = strstr(argv[i], "section=");
        if(temp != NULL) {
            sscanf(temp + 8, "%d", &secNr);
        }
        temp = strstr(argv[i], "line=");
        if(temp != NULL) {
            sscanf(temp + 5, "%d", &lineNr);
        }
    }

    secNr--;//they start from 0?

    File* toExtract = NULL;
    if(parseCommand(path, &toExtract) == -1) {
        printf("ERROR\ninvalid file\n");
        goto cleanup;
    }
    if(toExtract->nrSec <= secNr) {
        printf("ERROR\ninvalid section\n");
        goto cleanup;
    }
    
    int file = open(path, O_RDONLY);
    lseek(file, toExtract->section[secNr].offset, SEEK_SET);
    int  i = 0, sectSize = 0;
    while(i < lineNr - 1) {
        read(file, temp, 1);
        sectSize++;
        if(sectSize > toExtract->section[secNr].size) {
            printf("ERROR\ninvalid line");
            goto cleanup;
        }
        if(temp[0] == '\n') {
            i++;
        }
    }
    unsigned long int startOfSentence = lseek(file, SEEK_CUR, SEEK_CUR) - 1;
    do {
        read(file, temp, 1);
    } while(temp[0] != '\n');
    unsigned long int sentenceLength = lseek(file, SEEK_CUR, SEEK_CUR) - startOfSentence - 2;

    lseek(file, startOfSentence, SEEK_SET);
    char* line = (char*)malloc(sentenceLength * sizeof(char));
    read(file, line, sentenceLength);
    line[sentenceLength - 1] = '\0';
    printf("SUCCESS\n%s\n", line);
    cleanup:
    free(toExtract->section);
    free(line);
}

void findall(int argc, char **argv) {
    printf("WORK IN PPROGRESS\n");
}

void operation(int argc, char **argv){
    if (strcmp(argv[1], "variant") == 0){
        printf(VARIANT);
        printf("\n");
    }
    else if (strcmp(argv[1], "list") == 0){
        listCommand(argc, argv);
    }
    else if (strcmp(argv[1], "parse") == 0) {
        File *dump;
        parseCommand(argv[2] + 5, &dump);
        free(dump->section);
    }
    else if (strcmp(argv[1], "extract") == 0) {
        extract(argc, argv);
    }
    else if (strcmp(argv[1], "findall") == 0) {
        findall(argc, argv);
    }
    else {
        printf("INVALID COMMAND \"%s\"\n", argv[1]);
    }
}

int main(int argc, char **argv) {
    if (argc >= 2)
    {
        operation(argc, argv);
    }
    return 0;
}