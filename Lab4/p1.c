#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int get_line(int fd, int lineNr, char *line, int maxLength)
{
    char c[1];
    while(lineNr)
    {
        int x = read(fd, c, 1);
        if(x < 0)
        {
            printf("Can't read from file.\n");
            return -1;
        }
        if(x == 0)
        {
            printf("Unexpected end of file.\n");
            return -1;
        }
        if(c[0] == '\n')
        {
            lineNr--;
        }
    }
    
    int i;
    for(i = 0; i < maxLength; i++)
    {
        read(fd, line+i, 1);
        if(line[i] == 10)
        {
            break;
        }
    }
    if(i < maxLength) {maxLength = i;}

    if(line[maxLength] != 10)
    {
        printf("Line longer than expected.\n");
        return -1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    int file = -1;
    int line = -1;
    char *lineRead;
    int maxLength = -1;

    //filename lineNum length
    if(argc == 4)
    {
        file = open(argv[1], O_RDONLY);
        if(file == -1)
        {
            perror("Could not open input file");
            return -1;
        }
        printf("File opened successfuly.\n");
        if(sscanf(argv[2], "%d", &line) == 0)
        {
            printf("Invalid line number.\n");
            return -1;
        }
        if(sscanf(argv[3], "%d", &maxLength) == 0)
        {
            printf("Invalid length number.\n");
            return -1;
        }

        lineRead = (char*)malloc(maxLength * sizeof(char));

        if(!get_line(file, line, lineRead, maxLength))
        {
            printf("%s\n", lineRead);
        }

    } 
    else
    {
        printf("Insufficient arguments.\n");
        return -1;
    }

    return 0;
}